// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"


void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	auto ControlledTank = GetControllerTank();
	if (!ControlledTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("Playercontroller not possessing"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Playercontroller possessing tank"));
	}
	UE_LOG(LogTemp, Warning, TEXT("Playercontroller begin play"), *ControlledTank->GetName());
}

ATank* ATankPlayerController::GetControllerTank() const
{
	return Cast<ATank>(GetPawn());
} 