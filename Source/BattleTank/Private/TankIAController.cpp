// Fill out your copyright notice in the Description page of Project Settings.


//#include "Engine/World.h"
#include "TankIAController.h"
#include "Runtime/Engine/Classes/Engine/World.h"


void ATankIAController::BeginPlay()
{
	Super::BeginPlay();

	auto playerTank = GetPlayerTank();

	if (!playerTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("AI has not found player"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AI has found player"), *(playerTank->GetName()));
	}
}

ATank* ATankIAController::GetControlledTank() const
{
	return Cast<ATank>(GetPawn());
}

ATank* ATankIAController::GetPlayerTank() const
{
	auto playerTank = GetWorld()->GetFirstPlayerController()->GetPawn();

	if (!playerTank)
	{
		return nullptr;
	}
	return Cast<ATank>(playerTank);
}